package nl.rickmartens.antipluginsleak.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class ChatListener implements Listener {

    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent e) {
        Player p = e.getPlayer();

        if (p.hasPermission("antipluginsleak.bypass")) {
            return;
        }

        if (e.getMessage().startsWith("/plugins") || e.getMessage().startsWith("/bukkit:plugins") || e.getMessage().startsWith("/pl") || e.getMessage().startsWith("/bukkit:pl")) {
            e.setCancelled(true);
            p.sendMessage("§c§lYou are not allowed to see this.");
        } else if (e.getMessage().startsWith("/about") || e.getMessage().startsWith("/bukkit:about")) {
            e.setCancelled(true);
            p.sendMessage("§c§lYou are not allowed to see this.");
        } else if (e.getMessage().startsWith("/ver") || e.getMessage().startsWith("/bukkit:ver") || e.getMessage().startsWith("/version") || e.getMessage().startsWith("/bukkit:version")) {
            e.setCancelled(true);
            p.sendMessage("§c§lYou are not allowed to see this.");
        } else if (e.getMessage().startsWith("/?") || e.getMessage().startsWith("/bukkit:?")) {
            e.setCancelled(true);
            p.sendMessage("§c§lYou are not allowed to see this.");
        } else if (e.getMessage().startsWith("/help") || e.getMessage().startsWith("/bukkit:help")) {
            e.setCancelled(true);
            p.sendMessage("§c§lYou are not allowed to see this.");
        }
    }

}
